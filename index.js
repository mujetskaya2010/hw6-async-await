const btn = document.querySelector(".button");

btn.addEventListener("click", getRequst, {
  once: true,
});

async function getRequst(event) {
  const response = await fetch("https://api.ipify.org/?format=json");
  const result = await response.json();

  const responceiP = await fetch(
    "http://ip-api.com/json/" +
      result.ip +
      "?fields=continent,country,regionName,city,district"
  );
  const data = await responceiP.json();

  let target = event.target;
  target.insertAdjacentHTML(
    "afterend",
    `
    <ul>
    <li>${data.continent}</li>
    <li>${data.country}</li>
    <li>${data.regionName}</li>
    <li>${data.city}</li>
    <li>${data.district === "" ? "no data" : data.district}</li>
    </ul>
    `
  );
}
